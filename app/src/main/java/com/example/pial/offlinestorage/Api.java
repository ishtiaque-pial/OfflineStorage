package com.example.pial.offlinestorage;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Pial on 17-Jul-17.
 */

public interface Api {

    @GET("posts")
    Call<List<Article>> getValue();
}
